#!/system/bin/sh

# read the gpu dvfs table
FILENAME='/proc/gpufreq/gpufreq_opp_dump'

dvfs_table=()
length=0

IFS=$'\n'
for line in $(cat $FILENAME)
do
	echo $line
	### please check the table to check how to substring.
	dvfs_table[$length]=${line:12:6}
	echo ${dvfs_table[$length]}
	length=$(($length+1))
done

#### wait latency for each DVFS finish (0.1=100ms, 0.001=1ms)
T_DVFS_INTERVAL=0.05

while [ 1 ]
do
	sleep $T_DVFS_INTERVAL
	
	fix_opp=$(($RANDOM%$length))
	fix_opp=$(($fix_opp))
	echo ${dvfs_table[$fix_opp]} > /proc/gpufreq/gpufreq_opp_freq

	echo "fix_opp = $fix_opp, freq = ${dvfs_table[$fix_opp]}"
	
	cat /proc/gpufreq/gpufreq_opp_freq | grep -e freq

done

